﻿using System;

namespace TestProject
{

    public abstract class SuperClass
    {
        public string Name => GetType().Name;
    }

    public class ClassA : SuperClass { }
    public class ClassB : SuperClass { }


    class Program
    { 
        static void Main(string[] args)
        {
            ClassA a = new();
            ClassB b = new();

            Console.WriteLine(a.Name);
            Console.WriteLine(b.Name);
        }
    }


   

}
