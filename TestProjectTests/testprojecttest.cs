using Xunit;
using TestProject;


namespace TestProjectTests
{
    public abstract class SuperTest<T> where T: SuperClass, new()
    {
        [Fact]
        public void Name_Subclass()
        {
            string expected = typeof(T).Name;
            
            T classInstance = new();
            string actual = classInstance.Name;

            Assert.Equal(expected, actual);
        }
    }

    public class ClassAtest : SuperTest<ClassA> { }
    public class ClassBtest : SuperTest<ClassB> { }
}
